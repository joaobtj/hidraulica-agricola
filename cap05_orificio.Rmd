# Orifícios e bocais {#orificio}

Neste capítulo estudaremos o escoamento por orifícios e bocais.

## Coeficiente de descarga {#cd}

O jato de água que sai de um orifício ou bocal chama-se veia líquida. Sua trajetória é parabólica, como a de um corpo pesado animado de velocidade inicial. 

Experimentalmente, observa-se que os filetes líquidos tocam as bordas do orifício e continuam a convergir depois de passarem por este até uma seção A~2~, menor que a área do orifício, chamada seção contraída ou *vena contratca*. Um *Coeficiente de contração (C~c~)* é definido como a relação entre a área da seção contraída e a área do orifício. 

Ainda, é necessário aplicar um *Coeficiente de redução de velocidade (C~v~) *, que leva em conta as perdas existente, de modo que a velocidade real é sempre menor que a velocidade teórica. 

Obtemos, assim, o *Coefiente de descarga (C~d~)* pelo produto dos coeficentes de contração e redução da velocidade: $C_d=C_c \cdot C_v$.


## Teorema de Torricelli {#torricelli}

Aplicando o teorema de Bernoulli (Equação \@ref(eq:bernoulli2)) ao fluxo através de um pequeno orifício e tomando-se o eixo do orifício como referência, chegamos à Equação \@ref(eq:tor) conhecida como *teorema de Torricelli*.

\begin{equation}
  V = \sqrt{2 \cdot g \cdot h} 
  (\#eq:tor)
\end{equation}

em que h é a carga hidráulica, ou seja, a altura da coluna de fluido acima do eixo do orifício.

A velocidade real será sempre menor que a velocidade teórica, aplicando o *Coeficiente de redução de velocidade (C~v~)*. Assim, para situações reais, a equação \@ref(eq:tor) se torna:

\begin{equation}
  V = C_v \cdot \sqrt{2 \cdot g \cdot h} 
  (\#eq:tor2)
\end{equation}

Veja a deduçao do Teorema de Torricelli (Eq. \@ref(eq:tor) e \@ref(eq:tor2)) no vídeo a seguir.

```{r video-torricelli-deducao, echo=FALSE}

knitr::include_url("https://www.youtube.com/embed/xLzGDeihMtk")

```


## Orifícios {#orificio2}

Orifícios são perfurações de forma geométrica bem definida feitos abaixo da superfície livre do líquido em paredes de reservatórios. 

```{r 5-orificio, echo=FALSE, out.width=400, fig.cap="Representação de um orifício submetido a uma carga hidráulica *h*."}

knitr::include_graphics('images/5_orificio.png')

```

Podem ser classificados quanto à dimensão relativa:

+ Pequenos: menores que 1/10 da profundidade.
+ Grandes: maiores que 1/10 da profundidade.

No caso de orifícios pequenos, podemos admitir sem grande erros, que todas as partículas atravessam o orifício com a mesma velocidade, ou seja, a velocidade na parte superior e inferior são aproximadamente iguais.

Quanto à natureza da parede:

+ Parede delgada: quando o jato líquido apenas toca a perfuração em uma linha que constitui o perímetro do orifício.
+ Parede espessa: verifica-se a aderência do jato à parede.

Os orifícios em parede delgada são obtidos pelo corte em bisel ou quando a espessura da parede é inferior a 1,5 vez a dimensão do orifício. Acima de 1,5 e até 2 vezes a espessura da parede sem corte em bisel, o jato poderá se colar ao interior da parede, classificando como parede espessa. Acima de 2 e até 3 vezes a dimensão, teremos um bocal (Item \@ref(bocais))

```{r 5-orificio-classif, echo=FALSE, out.width=400, fig.cap="Orifício de parede delgada com corte em bisel (esquerda). Parede delgada e < 1,5D (centro). Parede espessa e  > 1,5D (direita)"}

knitr::include_graphics('images/5_orificio_classif.png')

```

## Orifícios pequenos em paredes delgadas {#orif-peq}

Em seu escoamento, podemos considerar que todas as partículas do fluido atravessam o orifício com a mesma velocidade. O coeficiente de redução da velocidade para orifício pequeno em parede delgada possui um valor prático médio de C~v~ = 0,985.

Para a vazão que escoa, precisamos da área do orifício, corrigida pelo Coeficiente de contração. Para orifícios pequenos em paredes delgadas, um valor prático médio é C~c~ = 0,62.

Logo, o Coeficiente de descarga para orifícios pequenos em paredes delgadas pode ser considerado, em termos práticos, C~d~ = 0,61.

Assim, podemos chegar a uma vazão escoando pelo orifício, combinando a definição do Coeficiente de descarga (C~d~) com o teorema de Torricelli (Equação \@ref(eq:tor) e com a equação \@ref(eq:vazao) da vazão.


\begin{equation}
  Q = Cd \cdot A \cdot \sqrt{2 \cdot g \cdot h} 
  (\#eq:orif-pq)
\end{equation}

em que A é a área do orifício.


```{example, orif-p, name="Orificio pequeno"}

Um orifício em parede delgada com 10 cm de diâmetro está instalado na parede de um grande reservatório. A altura da água acima do centro do orifício é de 5 m. Qual a velocidade média da água e a vazão do orifício?

```

```{solution}
\n
 
Pela equação \@ref(eq:tor2):
  
$V = 0,985 \cdot \sqrt{19,62 \cdot 5} = 9,76 m/s$
  
Pela equação \@ref(eq:orif-pq):
  
$Q = 0,61 \cdot \frac{\pi \cdot 0,10^2} {4} \sqrt{19,62 \cdot 5} = 45,5 L/s$

Veja a resolução no vídeo abaixo:
```

```{r video-orificio-p-exercicio, echo=FALSE}

knitr::include_url("https://www.youtube.com/embed/RJ6m78vJdqo")

```



## Orifício de grandes dimensões {#orif-grande}

Neste caso, não podemos admitir que todas as partículas possuem a mesma velocidade. Isso se deve ao fato de que a diferença na carga hidráulica em toda a área do orifício não pode ser desprezada. Logo, devemos usar a integral da equação \@ref(eq:orif-pq) em função da altura *"h"*.

Para orifícios retangulares, a vazão é dada pela equação \@ref(eq:orif-grq).

```{r 5-orificio-grande, echo=FALSE, out.width=450, fig.cap="Representação de um orifício de grandes dimensões D > 1/10h"}

knitr::include_graphics('images/5_orificio_grande.png')

```
\begin{equation}
  Q = \frac{2}{3} \cdot Cd \cdot L \cdot \sqrt{2 \cdot g} \cdot (h_2^{3/2} - h_1^{3/2})
  (\#eq:orif-grq)
\end{equation}

em que:  

+ L é a largura do orifício.
+ h~1~ é altura da água acima da base superior do orifício.
+ h~2~ é altura da água acima da base inferior do orifício.

```{example, orif-g, name="Orifício grande"}

Um orifício retangular com 60 cm de altura e 75 cm de largura está instalado em um grande reservatório. A altura da água acima do centro do orifício é de 50 cm. Qual a vazão do orifício?

```


```{solution}
\n

$Q = \frac{2}{3} \cdot 0,61 \cdot 0,75 \cdot \sqrt{19,62} \cdot (0,8^{3/2} - 0,2^{3/2}) = 0,85 m^3/s$

Veja a resolução do exercício no vídeo abaixo:
```

```{r video-orificio-g-exercicio, echo=FALSE}

knitr::include_url("https://www.youtube.com/embed/u2eKLoEwWdI")

```


## Bocais {#bocais}

Bocais são peças tubulares adaptadas aos orifícios. Para ser cosiderado um bocal, o seu comprimento deve estar compreendido entre 2 e 3 vezes a dimensão do orifício. Diversas configurações de bocais são possíveis.

A principal diferença dos bocais em relação aos orifícios está no coeficiente de descarga do bocal, que assume valores diferentes, frequentemente maiores, que o Cd do orifício. Alguns exemplos de bocais estão na Figura \@ref(fig:5-bocal).

```{r 5-bocal, echo=FALSE, out.width=300, fig.cap="Diferentes bocais: Bocal exterior (superior esquerdo). Bocal interno ou reentrante (superior direito). Bocal convergente (inferior esquerdo). Bocal divergente (inferior direito)." }

knitr::include_graphics('images/5_bocais.png')

```

Para um bocal cilíndrico externo, C~d~ = 0,81. Para um bocal reentrante, C~D~ = 0,51. O maior valor de C~d~ é alcançado com um bocal convergente com ângulo de 13°30', cujo C~d~ = 0,94.

A vazão dos bocais é calculada pelo mesmo método da vazão de orifícios (Equação \@ref(eq:orif-pq))


## Orifícios e bocais afogados {#afogado}

Diz-se que um orifício ou bocal está afogado quando a veia escoa em massa líquida. A expressão de Torricelli (Equação \@ref(eq:tor)) ainda é válida. No entanto, a carga hidráulica deve ser cosiderada como a diferença entre as cargas de montante e jusante.

Os coeficientes de descarga são ligeiramente inferiores comparadas a descarga livre, mas na mioria dos problemas práticos, essa diferença é desprezível.


```{r 5-afogado, echo=FALSE, out.width=300, fig.cap="Representação de um orifício afogado escoando dentro de uma massa líquida." }

knitr::include_graphics('images/5_afogado.png')

```


## Escoamento com nível variável {#nivel-var}

Nos casos considerados anteriormente, a carga hidráulica *"h"* era considerada constante. Na prática, essa situação nem sempre acontece. Se o nível não for mantido constante, a altura *"h"* irá diminuir com o tempo em consequência da própria vazão pelo orifício ou bocal. Com a redução da carga hidráulica, a vazão tambem irá decrescer. 

O problema que se apresenta na prática é determinar o tempo necessário  para o esvaziamento de um recipiente ou reservatório.

Para reservatórios com área constante (por exemplo, prisma, cilindro, paralelepípedo, etc.), o tempo de esvaziamento será:

\begin{equation}
  t = \frac{2 \cdot A_r}{C_d \cdot A_o \cdot \sqrt{2 \cdot g}}\cdot (\sqrt{h_1}-\sqrt{h_2})
  (\#eq:esvazia)
\end{equation}

em que:

+ A~r~ é a área do reservatório.
+ A~o~ é a área do orifício.
+ h~1~ é a altura inicial da água no reservatório.
+ h~2~ é a altura final da água no reservatório.

No caso de esvaziamento completo, h~2~ = 0, a equação \@ref(eq:esvazia) pode ser simplificada:

\begin{equation}
  t = \frac{2 \cdot A_r}{C_d \cdot A_o \cdot \sqrt{2 \cdot g}}\cdot \sqrt{h}
  (\#eq:esvazia0)
\end{equation}

<!-- ***deduzir a equação em video*** -->


```{example, esvazia, name="Esvaziamento"}

Considere um reservatório cilíndrico de 1,7 m de altura e 2,3 m de diâmetro com um bocal (C~d~ = 0,81) de 75 mm de diâmetro instalado no fundo deste reservatório.
  
  (a) Qual o tempo necessário para esvaziar metade do reservatório?

  (b) Qual o tempo necessário para o esvaziamento completo?

```

```{solution}
\n 

(a) Pela equação \@ref(eq:esvazia)

$t = \frac{(2 \cdot \pi \cdot 2,3^2/4)}{(0,81 \cdot \pi \cdot 0,075^2/4 \cdot \sqrt{19,62})}\cdot (\sqrt{1,7}-\sqrt{0,85}) = 200 s = 3,3 min$

(b) Pela equação \@ref(eq:esvazia0)

$t = \frac{(2 \cdot \pi \cdot 2,3^2/4)}{(0,81 \cdot \pi \cdot 0,075^2/4 \cdot \sqrt{19,62})}\cdot \sqrt{1,7} = 683 s = 11,4 min$

Veja o exercício resolvido abaixo:
  
```


```{r video-variavel-exercicio, echo=FALSE}

knitr::include_url("https://www.youtube.com/embed/YJkNHiUaS64
")

```
