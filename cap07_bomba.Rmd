# Estações de bombeamento {#bomba}

Uma estação de bombeamento, por vezes também chamada de estação elevatória, é utilizada quando a água tem que ser deslocada de um nível baixo para um nível mais elevado. 

## Tubulações de sucção e recalque

A tubulação é denominada de sucção quando se localiza antes da motobomba, e de recalque quando se localiza depois da motobomba.

Na tubulação de sucção, geralmente se encontram as seguintes peças:

* Válvula de pé e crivo, só permite a passagem do líquido no sentido ascendente. Tem a função de manter a bomba escorvada e atua como filtro de impurezas. 
* Curva de 90° devido ao traçado típico da tubulação.
* Redução que liga o final da tubulação de sucção à entrada da bomba, de diâmetro geralmente menor. Essa redução evita a formação de bolsas de ar na entrada da bomba

Na tubulação de recalque, geralmente encontramos as seguitnes peças:

* Ampliação concêntrica para ajuste do diâmetro.
* Válvula de retenção, unidirecional, que impede que o peso da coluna de água seja sustentado pela bomba, o que poderia desalinhá-la e impede o refluxo do líquido, fazendo a bomba funcionar como turbina, o que lhe provocaria danos. 
* Válvula de gaveta com função de regular a vazão e permitir reparos no sistema.

```{r 7-estacao, echo=FALSE, out.width=500, fig.cap="Configuração típica de uma estação de bombeamento."}

knitr::include_graphics('images/7-estacao.png')

```


### Dimensionamento das tubulações de recalque e sucção

O dimensionamento das tubulações de recalque e sucção trata da seleção do material das tubulações, disposição das tubulações no projeto e, principalmente, na seleção do diâmetro adequado dstas tubulações.

Existem vários critérios para a escolha do diâmetro das tubulações. Um critério amplamente utilizado é baseado na velocidade máxima da água na tubulação. Para tubulações de recalque, podemos adotar a velocidade máxima de 2,0 m/s. Para tubulações de sucção, esta velocidade é menor, de 1,0 m/s.

No entanto, é possível adotar um critério mais completo para as tubulaões de recalque, que considera a velocidade mínima e máxima da água nas tubulações. Adotar uma velocidade mínima evita o depósito de sedimentos, enquanto que não ultrapassar a velocidade máxima evita danos estruturais às tubulações. Assim, podemos dimensionar a tubulação de forma que a velocidade fique entre 0,7 m/s e 5,0 m/s. 

Assim:

\begin{equation} 
  D_r=\sqrt{\frac{4 \cdot Q}{\pi \cdot (0,7 \leq V \leq 5,0)}}
  (\#eq:Dr)
\end{equation} 


e

\begin{equation} 
  D_s=\sqrt{\frac{4 \cdot Q}{\pi \cdot 1,0}}
  (\#eq:Ds)
\end{equation} 

Os valores obtidos nas equações \@ref(eq:Dr) e  \@ref(eq:Ds) devem ser utilizados para seleção dos diâmetros comerciais que mais se aproximem dos diâmetros calculados.

<!-- Apesar deste critério da velocidade ser um bom ponto de partida para o dimensionamento, um estudo mais criterioso incluindo dos custos de instalação e de operação da estação de bomabemaneto levam a maior precisão no dimensionamento. Dessa forma, recomenda-se a análise de pelo menos 5 diâmetros comerciais (2 acima e 2 abaixo) do diãmetro comercial selecionado inicialmente no recalque.  -->

```{example, diametro, name="Escolha do diâmetro"}

Para uma vazão de requerida no projeto de 6,8 m^3^/h, quais devem ser os diâmetros das tubulações de recalque e de sucção?
  
```


```{solution}
\n

$$
D_r=\sqrt{\frac{4 \cdot 6,8/3600}{\pi \cdot 5,0}} \cdot 1000 = 21,9 mm
$$  
  
  
$$
D_r=\sqrt{\frac{4 \cdot 6,8/3600}{\pi \cdot 0,7}} \cdot 1000 = 58,6 mm
$$
  
O diâmetro da tubulação de recalque deve ficar entre 21,9 mm e 58,6 mm. Analisando a tabela de diâmetros comerciais abaixo, (ver Figura \@ref(fig:6-di-catalogo)), podemos escolher os diâmetros nominais entre 32 mm (Di = 27,8 mm) e 60 mm (Di = 53,4 mm).
  
$$
D_s=\sqrt{\frac{4 \cdot 6,8/3600}{\pi \cdot 1,0}} \cdot 1000 = 49,0 mm
$$

  
Para a sucção, escolhemos o diâmetro nominal imediatamente acima do valor calculado, ou seja, o de 60 mm (Di = 53,4 mm).



![](images/6-di-catalogo.png)



|   | Diâmetro nominal | Diâmetro interno |
|---|:----------------:|:----------------:|
| 1 |       32,00      |       27,80      |
| 2 |       40,00      |       35,20      |
| 3 |       50,00      |       44,00      |
| 4 |       60,00      |       53,40      |

O diâmetro da tubulação de sucção permanece inalterado (Dn=60mm, Di=53,4mm) para todas as opções. 

```

### Altura manométrica do sistema

Aplicando a equação do Teorema de Bernoulli, chegamos à conclusão que a altura manométrica de uma instalação é dada por:

\begin{equation} 
  H_m = H_g + hf
  (\#eq:Hm)
\end{equation} 


em que:

* H~g~- altura geométrica da instalação.
* hf - perdas de carga ocorridas na tubulação.

A altura geométrica é a diferença de altura entre o ponto mais baixo (captação) e o ponto mais alto (destino) do sistema.

As perdas de carga são aquelas de natureza contínua e localizada que ocorrem quando da movimentação da água pela tubulação.

```{example, hm, name="Altura manométrica."}

Considere uma instalaçao de bombeamento com as seguintes características:
  
* Vazão requerida = 6,8 m^3^/h
* Comprimento da tubulação de recalque = 18 m
* Comprimento da tubulação de sucção = 1,0 m
* Altura geométrica de recalque = 3,0 m
* Altura geométrica de sucção - 1,0 m
* Material da tubulação: PVC
* Peças especiais no recalque: 
  * ampliação
  * curva 90° (3x)
  * válvula de retenção
  * registro de gaveta
* Peças especiais na sucção:
  * válvula de pé e crivo
  * curva 90°
  * redução

Calcule a altura manométrica da instalação.

```


```{solution}
\n



Perda de carga contínua no recalque:

$hf_{cr} = \frac{6,107 \cdot 0,000135 \cdot (6,8/3600)^{1,75} \cdot 18}{0,0352^{4,75}} = 2,0357 mca$

Perda de carga localizada no recalque:

| Peça especial         | Coeficiente | Comprimento equivalente |
|-----------------------|:-----------:|:-----------------------:|
| Ampliação   gradual   |      10     |           0,40          |
| Válvula gaveta        |      8      |           0,32          |
| Cotovelo 90°          |      30     |           3,60          |
| Válvula de   retenção |     100     |           4,00          |
| TOTAL                 |             |           8,32          |

$hf_{lr} = \frac{6,107 \cdot 0,000135 \cdot (6,8/3600)^{1,75} \cdot 8,32}{0,0352^{4,75}} = 0,9410 mca$


Perda de contínua na sucção:

$hf_{cs} = \frac{10,643 \cdot (6,8/3600)^{1,852} \cdot 1}{140^{1,852} \cdot 0,0534^{4,871}} = 0,0161 mca$



Perda de carga localizada na sucção

| Peça especial           | Coeficiente | Comprimento equivalente |
|-------------------------|:-----------:|:-----------------------:|
| Redução   excêntrica    |      10     |           0,60          |
| Cotovelo 90°            |      30     |           1,80          |
| Válvula de pé   e crivo |     265     |          15,90          |
| TOTAL                   |             |          18,30          |

$hf_{ls} = \frac{10,643 \cdot (6,8/3600)^{1,852} \cdot 18,30}{140^{1,852} \cdot 0,0534^{4,871}} = 0,2941 mca$

Perda de carga total

$hf = 2,0357 + 0,9410 + 0,0161 + 0,2941 = 3,2869 mca$

Altura manométrica:

$H_m = (3 + 1) + 3,2869 = 7,29 mca$

```

### Curva característica do sistema {#cvsist}

É uma relação entre a vazão recalcada e a altura manométrica do sistema correspondente. Deve ser plotada em forma de um gráfico, e utiliza-se pelo menos 5 valores de vazão (2 acima e 2 abaixo da vazão requerida).

```{example}

Construa a curva característica do sistema do exemplo anterior.

```

```{solution}
\n

Utilizando 5 valores de vazão e calculando a respectiva altura manométrica para cada um, o resultado é:
  
|   Q  | Hm sistema |
|:----:|:----------:|
| 4,80 |    5,78    |
| 5,80 |    6,48    |
| 6,80 |    7,29    |
| 7,80 |    8,18    |
| 8,80 |    9,17    |
  
```

```{r, echo=FALSE}
hm_s <- c(5.78,6.48,7.29,8.18,9.17)
q_s <- c(4.8,5.8,6.8,7.8,8.8)

library(ggplot2)
  
ggplot(data.frame(hm_s,q_s), aes(x=q_s, y=hm_s)) +
  geom_line()+
  geom_point()+
  xlab('Vazão do sistema (m3/s)')+
  ylab('Altura manométrica do sistema (mca)')

```


## Bombas Hidráulicas

São máquinas que recebem trabalho mecânico e o transformam em energia hidráulica, ou seja, as bombas recebem energia de uma fonte externa e transferem essa energia para o líquido.

As bombas hidráulicas podem ser construídas em diferentes modelos para atender as mais diversas aplicações práticas. 

As mais comuns são as chamadas bombas centrífugas, bombas que fornecem energia ao fluido pelo aumento da velocidade deste em seu interior. 

A característica mais marcante das bombas hidráulicas cinéticas é que elas apresentam uma relação inversa entre vazão fornecida a altura total de bombeamento. 

* Quanto maior a altura de bombeamento menor a vazão fornecida pelo equipamento e vice-versa.
* Quanto maior a potência maior a vazão ou maior a altura de bombeamento.


```{r 7-bomba-corte, echo=FALSE, out.width=500, fig.cap="Vista em corte de uma bomba centrífuga."}

knitr::include_graphics('https://upload.wikimedia.org/wikipedia/commons/8/8c/Centrifugal_Pump_pt.png')

```


Em relação aos tipos de rotores, estes podem ser:

* Fechado: Para líquidos sem partículas em suspensão
* Semi-aberto: Incorpora uma parede no rotor para prevenir que matéria estranha se aloje no rotor e interfira na operação.
* Aberto: Palhetas montadas sobre o eixo. Vantagem: líquidos com sólidos em suspensão. Desvantagem: sofrer maior desgaste.


Em relação ao número de rotores:

* Simples estágio: apenas um rotor
* Múltiplos estágios: vários rotores operando em série,  que permitem o desenvolvimento de altas pressões

```{r 7-bomba-multi, echo=FALSE, out.width=500, fig.cap="Bomba centrífuga multiestágio."}

knitr::include_graphics('https://upload.wikimedia.org/wikipedia/commons/3/35/Multistage_centrifugal_pump.jpg
')

```


### Curva característica da bomba {#cvbomba}

Pode-se dizer que a curva característica da bomba é um retrato de seu funcionamento nas mais diversas situações. 
Elas são obtidas nas bancadas de ensaio dos fabricantes
As mais comuns são:

* $H_M=f(Q)$
* $NPSH=f(Q)$
* $Potencia=f(Q)$
* $η=f(Q)$

```{r 7-bomba-curva, echo=FALSE, out.width=500, fig.cap="Curvas características de uma bomba centrífuga."}

knitr::include_graphics('images/7-bomba-curva.png')

```

Analisando as curvas características de uma bomba centrífuga, observamos que a potência aumenta com a vazão. Assim, as bombas centrífugas devem ser ligadas com o registro fechado (Q=0), já que a potência necessária ao acionamento é mínima. 
Quando a altura manométrica diminui, aumenta a vazão e, consequentemente, a potência exigida para o funcionamento da bomba, o que poderá causar sobrecarga no motor.
O crescimento da altura manométrica não causa sobrecarga no motor das bombas centrífugas


### Seleção da bomba

Basicamente, a seleção de uma bomba para determinada situação é função da vazão a ser recalcada (Q) e da altura manométrica da instalação (HM).

A vazão a ser recalcada (Q) depende, essencialmente, do consumo diário da instalação, da jornada de trabalho da bomba e do número de bombas em funcionamento.

A altura manométrica da instalação (HM) depende do perfil topográfico do terreno que permite calcular o desnível geométrico, do comprimento e diâmetro das tubulações de sucção e recalque e do número de peças especiais dessas tubulações que permite o cálculo da perda de carga total.

Deve-se evitar um equívoco muito comum que é escolher a bomba pela potência, uma vez bombas com a mesma potência podem ter diferentes combinações de vazão e altura manométrica, que podem não ser adequadas ao projeto.



```{example}

Considere os seguintes valores de referência para seleção de uma motobomba:
  
* Q = 6,8 m^3^/h
* H~m~ = 7,29 mca

Escolha uma motobomba adequada para esta situação.

```


```{solution}
\n

Devemos procurar em uma catálogo de seleção de um fabricante os valores de vazão (Q) e altura manométrica (H~m~) que mais se aproximem dos valores de referência.

A figura abaixo indica uma escolha adequada:
  

```


```{r 7-bomba-selecao, echo=FALSE, out.width=800, fig.cap="Procedimento de seleção de uma motobomba."}

knitr::include_graphics('images/7-bomba-selecao.png')

```

Disponível em: [schneider.ind.br](https://schneidermotobombas.blob.core.windows.net/media/275094/schneider_tabela_selecao_2020-11.pdf) (página 29)


```{solution}
\n

O modelo escolhido é a motobomba BC-91 S/T 1/3 cv rotor 97 mm

A curva característica dessa motobomba já foi apresentada na Figura \@ref(fig:7-bomba-curva).

```

### Estudo conjunto das curvas características da bomba e do sistema

Sobrepondo as curvas características do sistema (Item \@ref(cvsist)) e da bomba (Item \@ref(cvbomba)), é possível definir graficamente o ponto de funcionamento, no local em que as curvas se cruzam.



```{r, echo=FALSE}
hm_s <- c(5.78,6.48,7.29,8.18,9.17)
q_s <- c(4.8,5.8,6.8,7.8,8.8)

hm_b <- 5:9
q_b <- c(8.0, 7.7, 7.3, 6.9, 6.5)

library(ggplot2)

ggplot(data.frame(hm_s, q_s, hm_b, q_b), aes(x = q_s, y = hm_s)) +
  geom_line() +
  geom_point() +
  geom_line(aes(x = q_b, y = hm_b)) +
  geom_point(aes(x = q_b, y = hm_b)) +
  geom_vline(xintercept = 7.1, color='blue') +
  geom_hline(yintercept = 7.55, color='blue') +
  xlab('Vazão (m3/s)') +
  ylab('Altura manométrica (mca)')

```

Nesta figura, as curvas se cruzam em Q = 7,1 m^3^/h e H~m~ = 7,55 mca.

### NPSH -  Net Positive Suction Head


O NPSH requerido por uma bomba é uma característica desse equipamento e é fornecido pelo fabricante. 

O NPSH disponível é função das características da instalação do sistema de bombeamento, e pode ser calculado por:

$$
NPSH_d = P_{atm} \pm h_s - hf_s - e
$$
em que:

* NPSH~d~ – carga hidráulica disponível na sucção
* P~atm~ – pressão atmosférica local
* h~s~ – altura geométrica de sucção
* hf~s~ – perda de carga na tubulação de sucção
* e - pressão de vapor da água a temperatura local

Para o bom funcionamento do sistema de bombeamento, é necessário que: **NPSH~d~ ≥ NPSH~r~**. 
Caso contrário ocorrerá um fenômeno denominado cavitação

Este fenômeno ocorre quand há a passagem da fase líquida para a fase gasosa em decorrência da diminuição da pressão do líquido. Esta situação acontece se a pressão na entrada da bomba é menor que a pressão de vapor da água naquela temperatura.

Na cavitação, há formação de bolhas d e onda de choque que golpeia as paredes da bomba e o rotor.

```{example}

Qual o NPSH disponível para a seguinte instalação?
  
* altitude ao nível do mar: P~atm~ = 10,33 mca  
* altura de sucção - h~s~ = 1,0 m
* perda de carga na sucção - hf~s~ = 0,341 mca
* pressão de vapor - e = 0,46 mca

```

```{solution}

$NPSH_d = 10,33 - 1,0 - 0,34 - 0,46 =  8,53 mca$


```

```{example}

Qual o NPSH requerido pela motobomba selecionada no exemplo anterior?

```





```{solution}
\n

O NPSH requerido é encontrado na curva característica em função da vazão no ponto de funcionamento.

![](images/7-bomba-npsh.png)

O NPSH requerido por esta bomba na vazão de 7,1 m^3^/h é de 5,0 mca.

Sendo o NPSH disponível igual a 8,53 mca, este é maior que o NPSH requerido (5,0 mca). Dessa forma, a bomba *não* irá cavitar.

```





### Potência do conjunto motobomba

A potência requerida por um sistema de bombeamento pode ser calculada por:

$$
Pot = \frac{\gamma \cdot Q \cdot H_m}{\eta}
$$

em que:

* Pot – potência requerida, W (1 cv=735 W)
* $\gamma$γ – peso específico do fluido, N/m3
* Q – vazão a ser bombeada, m3/s
* H~m~ – altura manométrica, m
* $\eta$ (eta) – rendimento da bomba, decimal

A potência e o rendimento $\eta$ (eta) também são encontrados nas curvas carecterísticas da bomba.


```{example}

Qual a potência consumida pelo conjunto motobomba:
  
* Vazão no ponto de funcionamento: Q = 7,1 m^3^/h 
* Altura manométrica no ponto de funcionamento:  H~m~ = 7,55 mca.
* Rendimento: 0,42



```




```{solution}

$Pot = \frac{9810 \cdot 7,1/3600 \cdot 7,55}{0,47} = 347,7 W = 0,47 cv$
  
A potência encontrada na curva característica da potência é bastante próximo ao valor calculado pela equação acima.

![](images/7-bomba-pot-rend.png)

```

Importante ressaltar que a potência de 1/3 cv (0,33 cv), que caracteriza o modelo da motobomba selecionado, é a potência nominal. A potência consumida, que é relativa ao ponto em que o conjunto vai operar, poderá ser ligeiramente diferente (maior ou menor), neste caso, 0,47 cv. 


### Associação de bombas

A associação de bombas é necessária quando se verifica a inexistência no mercado de bombas que possam, isoladamente, atender a demanda de vazão ou de altura manométrica do sistema. Outra situação ocorre quando as demandas variam com o tempo.


Associação em paralelo: as vazões somam-se para a mesma altura manométrica

```{r 7-bomba-paralelo, echo=FALSE, out.width=300, fig.cap="Associaçao em paralelo"}

knitr::include_graphics('images/7-bomba-paralelo.png')

```

Associaçao em série: as alturas manométricas somam-se para uma mesma vazão

```{r 7-bomba-serie, echo=FALSE, out.width=300, fig.cap="Associaçao em série."}

knitr::include_graphics('images/7-bomba-serie.png')

```

## Análise econômica de projetos de hidráulica

A análise econômica pode ser o principal fator de escolha entre diferentes projetos com características semelhantes. Um bom critério para a escolha do projeto pode ser aquele que possuir o menor custo anual total.

O custo total anual é composto pela soma dos custos de investimento e custos de operação. 

Os custos de investimento se referem à aquisição das tubulações e do conjunto motobomba. Os snvestimentos são feitos no momento da instalação da estação de bombeamento e incidem antes de iniciar a operação do sistema. Geralmente é caracterizado como um pagamento único.

Os custos de operação são devidos ao consumo de energia elétrica ou combustível. Este custo incide somente depois de iniciada operação do sistema, e varia conforme o tempo de funcionamento deste. Geralmente é expresso em termos de pagamentos anuais.

Outros custos podem ser incluídos em uma análise mais detalhada, como os custos de
manutenção, instalação, putras peças, etc. De modo geral, estes agem uniformemente sobre o projeto, de modo que podem ser desconsiderados sem maiores problemas.

Devido à diferença nas características dos custos de investimento e dos custos de operação, devemos realziar uma transformação para que seja possivel a comparação entre eles. Um escolha é transformar o custo do investimento inicial em uma série uniforme de pagamentos ao longo da vida útil do projeto e considerando uma taxa de juros sobre o investimento.

$$
CA = INV \cdot \frac{(1+j)^n \cdot j}{(1+j)^n-1}
$$

em que:

* CA é o custo anual em R$
* INV é o investimento inicial em R$
* n é a vida útil do projeto, em anos
* j é a taxa de juros anual, em decimal


