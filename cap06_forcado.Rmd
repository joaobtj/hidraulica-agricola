# Condutos forçados {#forcado}

São canalizações em que o escoamento ocorre em uma pressão diferente da pressão atmosférica. Para isso, a canalização é fechada e a seção deverá estar completamente preenchida. Pode ocorrer por gravidade ou com bombeamento.

## Perda de carga 

Nos fluidos em movimento, observa-se que parte da energia é dissipada na forma de calor, denominando-se perda de carga (hf).

A resistência ao escoamento no caso de regime laminar é devida inteiramente à viscosidade, e ocorre devido ao atrito interno do fluido. Não se deve supor que este atrito seja igual ao verificado em sólidos. Junto às paredes do tubo, não há movimento do fluido. A velocidade se eleva de zero até seu valor máximo junto ao eixo do tubo. Pode-se, assim, imaginar uma série de camadas em movimento, com velocidades diferentes e responsáveis pela dissipação da energia.

Quando o escoamento se faz em regime turbulento, a resistência é o efeito combinado das forças devido à viscosidade e à inércia. Nesse caso, a distribuição de velocidade na canalização depende da turbulência, maior ou menor, e esta é influenciada pelas condições da parede do tubo. Um tubo com paredes mais rugosas causaria maior turbulência. 


## Extensão do Teorema de Bernoulli aos casos práticos {#bernoulli-pratico}

A experiência não confirma rigorosamente o Teorema de Bernoulli, isto porque os fluidos reais se afastam do modelo perfeito. A viscosidade e o atrito são os principais responsáveis pela diferença, o escoamento somente ocorre com uma perda de energia: a perda de carga.

Por isso, se introduz na equação de Bernoulli um termo corretivo hf (perda de carga):

$$
  \frac{P_1}{\gamma} + \frac{V_1^2}{2g} + z_1 = \frac{P_2}{\gamma} + \frac{V_2^2}{2g} + z_1 + hf_{1 \rightarrow 2}  
$$


O enunciado do Teorema de Bernoulli fica sendo, portanto:

> Para um escoamento permanente, a carga total de energia em qualquer ponto de uma linha de corrente é igual a carga total em qualquer ponto a jusante da mesma linha de corrente somada a perda de carga entre os dois pontos.



```{r 6-bern, echo=FALSE, out.width=500, fig.cap="Representação do Teorema de Bernoulli aplicado a fluidos reais."}

knitr::include_graphics('images/6-bern.jpg')

```

Na figura acima, podemos verificar:

* As distâncias dos pontos 1 e 2 ao plano de referência correspondem às cargas de posição z~1~ e z~2~.
* Marcando-se acima dos pontos 1 e 2 as respectivas cargas piezométricas ($\frac{P}{\gamma}$) fica definida a linha piezométrica
* A soma $\frac{P}{\gamma} + z$ representa a energia potencial do fluido em relação ao referencial adotado
* Adicionando o termo cinético (\frac{V^2}{2g}) se obtém a linha de energia do escoamento.
* A energia em 1 é maior que em 2 uma quantidade igual a perda de carga, energia dissipada irreversivelmente.

```{example, hf1, name="Perda de carga 1"}

A água flui de um reservatório até um aspersor funcionando com uma vazão de 5 m^3^/h e uma pressão de serviço de 3 kgf/cm^2^. Tendo a tubulação um diâmetro de 25 mm, e a altura h=50 m, calcule a perda de carga (hf) entre A e B.

```

```{r 6-hf1, echo=FALSE, out.width=300, fig.cap=" "}

knitr::include_graphics('images/6-hf1.png')

```


```{solution}
\n

Velocidade:
  
$V_B = \frac{4 \cdot 5/3600}{\pi \cdot 0,025^2} = 2,83 m/s$

Perda de carga: 
  
$0 + 0 + 50 = 30 + \frac{2,83^2}{19,62} + 0 + hf_{A \rightarrow B}$
  
$hf_{A \rightarrow B} = 19,6 mca$

```




```{example, hf2, name="Perda de carga 2"}

O diâmetro de uma tubulação aumenta gradualmente de 150 mm em B para 450 mm em A, estando o ponto A 4,5 m abaixo do ponto B. Se a pressão em A for de 0,7 kgf/cm^2^ e em B de 0,490 Kgf/cm^2^, e a descarga de 140 L/s, qual o sentido do escoamento e a perda por atrito entre os dois pontos?
  
```

```{solution}
\n

Velocidade:
  
$V_A = \frac{4 \cdot 0,140}{\pi \cdot 0,450^2} = 0,88 m/s$
  
$V_B = \frac{4 \cdot 0,140}{\pi \cdot 0,150^2} = 7,92 m/s$
  
Perda de carga:
  
$7 + \frac{0,88^2}{19,62} + 0 = 4,9 + \frac{7,92^2}{19,62} + 4,5 + hf_{A \rightarrow B}$
  
$hf_{A \rightarrow B} = -5,56 mca$
  
$hf_{B \rightarrow A} = +5,56 mca$
  
Sentido de B para A

```


```{example, hf3, name="Perda de carga 3"}

A canalização inclinada esquematizada na figura é composta por 2 trechos de diâmetro 50 mm e 75 mm cuja diferença de altura é 1,0 m. Analisando a deflexão da coluna de mercúrio do manômetro diferencial e sabendo que a canalização conduz água a uma vazão de 5 L/s, determine o sentido do escoamento e a perda de carga no trecho AB. 

```


```{r 6-hf3, echo=FALSE, out.width=300, fig.cap=" "}

knitr::include_graphics('images/6-hf3.png')

```

```{solution}
\n

Diferença de pressão:
  
$P_A + 0,5 \cdot 13,6 = P_B + 0,5 \cdot 1 + 1$
  
$P_B - P_A = 5,3 mca$
  
Admintindo uma pressão qualquer no ponto A, digamos, P~A~=0, então, P~B~=5,3 mca.
  
$V_A = \frac{4 \cdot 0,005}{\pi \cdot 0,050^2} = 2,55 m/s$

$V_B = \frac{4 \cdot 0,005}{\pi \cdot 0,075^2} = 1,13 m/s$  
  

$0 + \frac{2,55^2}{19,62} + 0 = 5,3 + \frac{1,13^2}{19,62} + 1 + hf_{A \rightarrow B}$
  
$hf_{A \rightarrow B} = -6,03 mca$
  
$hf_{B \rightarrow A} = +6,03 mca$
  

Sentido de B para A



```

Neste vídeo está a solução detalhada desta série de exercícios:

```{r video-perda_carga, echo=FALSE}

knitr::include_url("https://www.youtube.com/embed/7KCtI2w5rTM")

```





## Perda de carga contínua

Poucos problemas mereceram tanta atenção ou foram tão investigados quanto o da determinação das perdas de carga em uma tubulação. As dificuldades que se apresentam os estudos analíticos da questão são tantas que levaram os pesquisadores às investigações experimentais. Assim foi que, após inúmeras experiências conduzidas por Darcy e outros investigadores, com tubos de seção circular, concluiu-se que a resistência ao escoamento é:

*	Proporcional ao comprimento (L) da canalização
* Inversamente proporcional ao diâmetro (1/D)
*	Proporcional a uma potência da velocidade (V^n^)
*	Variável com a natureza dos tubos (rugosidade)
*	Independente da posição dos tubos
*	Independente da pressão interna

### A Fórmula Universal

Obtém-se assim, a fórmula de cálculo de tubulações denominada Fórmula de Darcy-Weisbach ou ainda, Fórmula Universal:
 
\begin{equation} 
  hf = f \frac{L}{D} \frac{V^2}{2g}
  (\#eq:universal0)
\end{equation} 

em que: 

* hf – perda de carga contínua, mca
* f – fator de atrito
* L – comprimento da tubulação, m
* D – diâmetro da tubulação, m
* V – velocidade do fluido, m/s

A dificuldade na utilização da Fórmula Universal está na obtenção do coeficiente de atrito (f), pois este é uma função da rugosidade do tubo, da viscosidade e densidade do fluido, da velocidade e diâmetro da tubulação.

Combinando a equação apresentada acima com a equação da continuidade (Q=A.V), esta pode ser escrita como:
 
\begin{equation} 
  hf = f \frac{8}{\pi^2 \cdot g} \frac{Q^2 \cdot L}{D^5}
  (\#eq:universal)
\end{equation} 
	 
A Fórmula universal é aplicável aos problemas de escoamento de qualquer líquido (água, óleo, gasolina, etc.) em encanamentos. 

### Fórmulas para fator de atrito (f) {#fator-f}

***Regime laminar***

No caso do regime laminar (Re<2000), o coeficiente de atrito é calculado por:
 
 
\begin{equation} 
  f = \frac{64}{Re}
  (\#eq:flaminar)
\end{equation} 
 
A perda de carga no regime laminar não depende da rugosidade das paredes do tubo.

***Regime turbulento***

Para o regime turbulento, podemos utilizar a equação de Colebrook-White (1939), bastante citada na literatura com bons resultados práticos:

\begin{equation} 
  \frac{1}{\sqrt{f}} = -2 \cdot log \left ( \frac{e}{3,7 \cdot D} + \frac{2,51}{Re \cdot \sqrt{f}} \right )
  (\#eq:fturbulento)
\end{equation} 
 
Essa equação é válida para qualquer faixa do regime turbulento. Seu inconveniente é precisar de métodos iterativos para calcular o valor de f.

***1º caso: Conhecidos Q, L, D, υ, ε. Incógnita: hf***
Considere

$x_2 = x_1 - \frac{x_1 + 2 \cdot log\left (\frac{e}{3,7 \cdot D} + \frac{2,51 \cdot x_1}{Re}  \right ) }{1 + \left ( \frac{2,18}{ \left ( \frac{e \cdot Re}{3,7 \cdot D} + 2,51 \cdot x_1 \right )} \right ) }$

1.	Calcule a velocidade pela equação da continuidade
2.	Calcule Re
3.	Atribua um valor inicial para x~1~, por exemplo, x~1~= 4 
4.	Calcule o valor de x~2~
5.	Verifique se x~2~ = x~1~; se for igual, vá ao passo seguinte, caso contrário, volte ao passo 4 utilizando o valor de x~2~ no lugar de x~1~
6.	Calcule o valor de f
7.	Calcule a perda de carga pela fórmula universal.


***2º caso: Conhecidos hf, L, D, υ, ε. Incógnita: Q ou V***

Considere:

$Re \cdot \sqrt{f} = \frac{D}{\nu} \sqrt{(2g \cdot D \cdot hf/L)}$

$f =  \left ( \frac{1}{-2 \cdot log  \left ( \frac{e}{3,7 \cdot D} + \frac{2,51}{Re \cdot \sqrt{f}} \right ) } \right ) ^2$

1.	Calcule $Re\sqrt{f}$ (primeira equação)
2.	Calcule f (segunda equação)
3.	Calcule V pela fórmula universal
4.	Calcule Q pela equação da continuidade


***3º caso: Conhecidos hf, Q, L, υ, ε. Incógnita: D***


Considere

$p = \frac{2 \cdot \sqrt{12,1 \cdot hf/L}}{Q}$

$q = \frac{e}{3,7}$

$r = \frac{2,51 \nu}{\sqrt{2g \cdot hf/L}}$

$x_2 = x_1 - \frac{x_1^5 + p \cdot log (q\cdot x_1^2 + r \cdot x_1^3)}{5 \cdot x_1^4 + p \cdot log (e) \left (\frac{2 \cdot q + 3 \cdot r \cdot x_1^3}{q \cdot x_1 + r \cdot x_1^2} \right )}$

$D = \frac{1}{x^2}$

1.	Calcule os valores das variáveis p, q e r 
2.	Atribua um valor para x1, por exemplo, x~1~ = 0,1
3.	Calcule o valor de x~2~ 
4.	Verifique se x~2~ = x~1~; se for igual, vá ao passo seguinte, caso contrário, volte ao passo 2 utilizando o valor de x~2~ no lugar de x~1~
5.	Calcule o diâmetro


### Fórmula de Hazen-Willians

Após cuidadoso exame estatístico de milhares de dados experimentais, dois pesquisadores norte-americanos propuseram, em 1903, uma fórmula empírica que ficou conhecida como fórmula de Hazen-Willians (Allen Hazen, engenheiro civil e sanitarista; Gardner S. Willians, professor de Hidráulica) que goza de grande aceitação devido ao amplo uso e às confirmações experimentais.

Adicionando a equação da continuidade, a fórmula de Hazen-Willians pode ser escrita, em unidades no SI, da seguinte forma:


\begin{equation} 
  hf = \frac{10,643 \cdot Q^{1,852} \cdot L}{C^{1,852} \cdot D^{4,871}}
  (\#eq:HW)
\end{equation} 

em que:

* hf – perda de carga, m
* Q – vazão, m^3^/s
* L - comprimento, m
* C – coeficiente de atrito de Hazen-Willians, adimensional
* D – diâmetro, m


Como toda fórmula empírica, ela só pode ser usada para os casos em que foi deduzida, são eles:

*	Água a temperatura ambiente, cerca de 20°C
*	Regime turbulento
*	Diâmetro entre 50 e 3500 mm

A tabela abaixo apresenta o valor do coeficiente de atrito de Hazen-Willian para alguns materiais:

|     Material    | Coeficiente C |
|:---------------:|:-------------:|
|  Aço corrugado  |       60      |
| Aço galvanizado |      125      |
|      Cobre      |      140      |
|     Concreto    |      130      |
|      Ferro      |      130      |
|     Plástico    |      140      |


A fórmula de Hazen-Willians requer, para sua aplicação criteriosa, grande cuidado na adoção do coeficiente C. A escolha negligente desse coeficiente, ou a fixação de um valor médio invariável reduz muito a precisão que pode ser esperar de tal fórmula.

```{example, hw1, name="Hazen-Willian 1"}

Uma adutora constituída por tubos de PVC (C=140) de 150 mm deverá fornecer 25 L/s de água a uma propriedade agrícola. Se o comprimento da adutora é de 1000 m, determinar o desnível (hf) necessário à obtenção desta vazão.

```


```{solution}
\n

$hf = \frac{10,643 \cdot 0,025^{1,852} \cdot 1000}{140^{1,852} \cdot 0,150^{4,871}} = 12,55 mca$

```

```{example, hw2, name="Hazen-Willian 2"}

Calcular a vazão fornecida por uma adutora construída com 3200 m de tubos de PVC (C=140) com diâmetro de 200 mm. A adutora é alimentada por um reservatório, cujo nível está situado na cota 140 m descarregando em outro reservatório cujo nível de água se situa na cota 92 m. 

```


```{solution}
\n

$Q = \left ( \frac{hf \cdot C^{1,852} \cdot D^{4,871}}{10,643 \cdot L} \right )^{\frac{1}{1,852}}$
  
$Q = \left ( \frac{48 \cdot 140^{1,852} \cdot 0,200^{4,871}}{10,643 \cdot 3200} \right )^{\frac{1}{1,852}} = 0,059 m^3/s$
  
```




```{example, hw3, name="Hazen-Willian 3"}

Dois reservatórios, cujos níveis de água estão situados nas cotas 195 m e 100 m, estão interligados por meio de uma tubulação de PVC (C=140) de 975 m de comprimento que conduz uma vazão de 5 L/s. Calcular o diâmetro interno dos tubos da adutora de interligação.

```



```{solution}
\n

$D = \left ( \frac{10,643 \cdot Q^{1,852} \cdot L}{hf \cdot C^{1,852}} \right ) ^ {\frac{1}{4,871}}$
  
 
$D = \left ( \frac{10,643 \cdot 0,005^{1,852} \cdot 975}{95  \cdot 140^{1,852}} \right ) ^ {\frac{1}{4,871}} \cdot 1000 = 53,4 mm$
  
```




No vídeo você pode conferir a resolução destes exercícios:

```{r video-hw, echo=FALSE}

knitr::include_url("https://www.youtube.com/embed/1yOPeK-dEFw")

```



### Fórmula de Flamant

Aplica-se ao caso em que a fórmula de Hazen-Willians não deve ser utilizada, principalmente para diâmetros menores de 50 mm.

\begin{equation} 
  hf = \frac{6,107 \cdot b \cdot Q^{1,75} \cdot L} {D^{4,75}}
  (\#eq:flamant)
\end{equation} 

Abaixo alguns valores para o coeficiente de rugosidade "b" de Flamant:

|           Material          |     Coeficiente b |
|:---------------------------:|:-----------------:|
|      Ferro fundido novo     |      0,000185     |
|     Aço galvanizado novo    |      0,000185     |
|        Cimento amianto      |      0,000155     |
|             Plástico        |      0,000135     |



## Perda de carga localizada

A perda de carga localizada é aquela provocada pelas peças especiais e demais singularidades presentem em uma tubulação.

Há várias maneiras para efetuar seu cálculo, sendo uma das mais simples o métodos dos comprimentos virtuais.

Uma canalização que compreende diversas peças especiais e outras singularidades, sob o ponto de vista de perdas de carga, equivale a um encanamento retilíneo de comprimento maior. O método consiste em se adicionarem à extensão da canalização, para simples efeito de cálculo, comprimentos tais que correspondam à mesma perda de carga que causariam as peças especiais existentes na canalização. A cada peça especial corresponde um certo comprimento fictício e adicional. Levando-se em consideração todas as peças especiais e demais causas de perda, chega-se a um comprimento virtual de canalização. 


```{r 6-virtual, echo=FALSE, out.width=300, fig.cap="Representação da perda de carga localizada pelo método dos comprimentos virtuais."}

knitr::include_graphics('images/6-virtual.png')

```

Para a determinação dos comprimentos virtuais ou equivalentes, podemos utilizar coeficientes expressos em número de diâmetros da tubulação, uma vez que verifica-se uma relação entre o comprimento equivalente (Leq) das peças especiais e o diâmetro.


|            Peça           | Comprimento equivalente (x diâmetros)    |
|:-------------------------:|:----------------------------------------:|
|         Curva 90°         |                    30                    |
|         Curva 45°         |                    15                    |
|       Entrada normal      |                    16                    |
|      Entrada de borda     |                    35                    |
| Registro de gaveta aberto |                     8                    |
|  Registro de globo aberto |                    350                   |
|    Saída de canalização   |                    35                    |
|     Tê passagem direta    |                    20                    |
|      Tê saída lateral     |                    50                    |
|     Tê saída bilateral    |                    65                    |
|    Válvula de retenção    |                    100                   |
|   Válvula de pé e crivo   |                    265                   |



```{example, localizada, name="Comprimento equivalente"}

Considere a instalação abaixo.

Calcular as perdas de carga para uma vazão do chuveiro de 4 L/min. Considerar a tubulação com 5 m de comprimento e diâmetro de 3/4". 

```

```{r 6-localizada, echo=FALSE, out.width=300, fig.cap=" "}

knitr::include_graphics('images/6-loc.png')

```




```{solution}
\n

Perda de carga contínua:
  
$hf_{con} = \frac{6,107 \cdot 0,000135 \cdot (4/60000)^{1,75} \cdot 5}{0,019^{4,75}} = 0,0304 mca$

Perda de carga localizada:
  

| | Peça                   |  L virtual (m)  |
|-|------------------------|:---------------:|
|1| Entrada de canalização |    16 x 0,019   |
|2| Curva 90°              |    30 x 0,019   |
|3| Curva 90°              |    30 x 0,019   |
|4| Te passagem direta     |    20 x 0,019   |
|5| Te passagem direta     |    20 x 0,019   |  
|6| Te saída lateral       |    50 x 0,019   |
|7| Registro de gaveta     |     8 x 0,019   |
|8| Curva 90°              |    30 x 0,019   |
| | TOTAL                  |       3,876     |


$hf_{loc} = \frac{6,107 \cdot 0,000135 \cdot (4/60000)^{1,75} \cdot 3,876}{0,019^{4,75}} = 0,0234 mca$

  


hf~t~ = hf~loc~ + hf ~con~
  
hf~t~ = 0,0304 + 0,0234 = 0,0538 mca
  
```

```{r video-loc, echo=FALSE}

knitr::include_url("https://www.youtube.com/embed/I0vjQ-2PF3o")

```




## Diâmetros comerciais

Não são todos os diâmetros de tubulações que podem ser encontrados no mercado. Existe um número limitado de diâmetros comerciais. 

Os diâmetros calculados em projetos devem ser aproximados para um diâmetro comercial existente. Esta aproximação, para um diâmetro inferior ou superior deve levar em consideração as especificidades do projeto.


Os diâmetros comerciais são padronizados por normas técnicase sempre se deve considerar o diâmetro interno para o cálculo da perda de carga e escolha da tubulação.

Para tubos de PVC rígido, o diâmetro externo é padronizado. Por isso, o diâmetro interno deve ser calculado com a fórmula abaixo:


$D_i = D_e - 2 \cdot e$

em que:

* D~i~ - diâmetro interno
* D~e~ - diâmetro externo
* e - espessura da parede do tubo



```{r 6-di, echo=FALSE, out.width=500, fig.cap="Representaçao do diâmetro interno de uma tubulação."}

knitr::include_graphics('images/6-di.jpg')

```


Estas medidas são bastante comuns em catálogos comerciais de tubulações:

```{r 6-di-catalogo, echo=FALSE, out.width=500, fig.cap="Exemplo de catálogo de tubulações de PVC."}

knitr::include_graphics('images/6-di-catalogo.png')

```

Fonte: [amancowavin.com.br](http://amancowavin.com.br/)



## Condutos equivalentes

Dois ou mais condutos hidráulicos são considerados equivalentes quando fornecem a mesma vazão para uma mesma perda de carga. Em muitas situações práticas, é conveniente a utilização de condutos equivalentes de forma a atender as limitações de perda de carga ou à disponibilidade de diâmetros comerciais.



### Condutos em série

Neste tipo de equivalência, a vazão conduzida pela tubulação é a mesma em todos os seus trechos de diferentes diâmetros, enquanto que a perda de carga total é igual a soma das perdas de carga parciais.


$L = L_1 + L_2 + ... + L_i$

$Hf = hf_1 + hf_2 + ... + hf_i$

$Q = Q_1 = Q_2 = ... = Q_i$

Generalizando a expressão:

$\frac{L}{D^{2m+n}} = \frac{L_1}{D_1^{2m+n}} + \frac{L_2}{D_2^{2m+n}} + ... + \frac{L_i}{D_i^{2m+n}}$

Onde m e n são, respectivamente, o expoente do termo velocidade e do diâmetro na fórmula utilizada para o cálculo da perda de carga no conduto.

|                |   m   |   n   |  2m+n |
|----------------|:-----:|:-----:|:-----:|
| Universal      |   2   |   1   |   5   |
| Hazen-Willians | 1,852 | 1,167 | 4,871 |
| Flamant        |  1,75 |  1,25 |  4,75 |


```{example equiv-serie, name="Equivalente em série"}

Dimensionar uma adutora de PVC utilizando dois diâmetros comerciais em série, para atender as especificações do projeto abaixo. 

* Vazão: 25 L/s
* Comprimento: 1000 m
* Perda de carga permitida: 25 mca

Utilizar a fórmula de Hazen-Willians adotando C=140.
```

```{solution}
\n


$D = \left ( \frac{10,643 \cdot Q^{1,852} \cdot L}{hf \cdot C^{1,852}} \right ) ^ {\frac{1}{4,871}} = \left ( \frac{10,643 \cdot 0,025^{1,852} \cdot 1000}{25 \cdot 140^{1,852}} \right ) ^ {\frac{1}{4,871}} \cdot 1000 = 130,9 mm$

Diâmetros comerciais: 
  
* 150 mm D~i~ = 161,2 mm
* 100 mm D~i~ = 111,8 mm

Sistema de equações:

1ª equação:

$L = L_1 + L_2$

Resolvendo a 1ª equação:

$1000 = L_1 + L_2$

$L_1 = 1000 - L_2$

2ª equação:

$\frac{L}{D^{2m+n}} = \frac{L_1}{D_1^{2m+n}} + \frac{L_2}{D_2^{2m+n}}$

Resolvendo a 2ª equação:

$\frac{1000}{0,1309^{4,871}} = \frac{L_1}{0,1118^{4,871}} + \frac{L_2}{0,1612^{4,871}}$

Substituindo a 1ª equação na 2ª equação:

$\frac{1000}{0,1309^{4,871}} = \frac{1000 - L_2}{0,1118^{4,871}} + \frac{L_2}{0,1612^{4,871}}$

Resolvendo:

$L_2 = 508 m$ 

$L_1 = 492 m$

```


```{r video-equivalente-serie, echo=FALSE}

knitr::include_url("https://www.youtube.com/embed/IacDPgLyOc4")

```



### Condutos em paralelo

Em um sistema paralelo equivalente, a vazão total é distribuída proporcionalmente ao diâmetro dos ramais (desde que os tubos sejam iguais).

$hf = hf_1 = hf_2 = ... = hf_i$

$Q = Q_1 + Q_2 + ... + Q_i$

Generalizando a expressão:

$\left(\frac{D  ^{2m+n}}{L  }\right)^{\frac{1}{m}} = \left(\frac{D_1^{2m+n}}{L_1}\right)^{\frac{1}{m}} + \left(\frac{D_2^{2m+n}}{L_2}\right)^{\frac{1}{m}} +  ... + \left(\frac{D_i^{2m+n}}{L_i}\right)^{\frac{1}{m}}$


```{example equiv-paralelo, name="Equivalente em paralelo"}

Um sistema de adução de água interliga dois reservatórios cuja diferença de nível é de 12 m. As adutoras são constituídas de tubos de PVC (C=140). 

* A adutora 1 tem 60 m de comprimento e diâmetro de 60 mm. (D~i~ = 56 mm) 
* A adutora 2 tem 80 m de comprimento de diâmetro de 75 mm. (D~i~ = 71 mm)
* A adutora 3 tem 120 m de comprimento de diâmetro de 100 mm. (D~i~ = 96 mm)

Qual a vazão do sistema? Usar a fórmula de Hazen-Willians.

```



```{solution}
\n


$\left(\frac{D ^{4,871}}{120}\right)^{0,54} = \left(\frac{0,056^{4,871}}{60}\right)^{0,54} + \left(\frac{0,071^{4,871}}{80}\right)^{0,54} +   \left(\frac{0,096^{4,871}}{120}\right)^{0,54}$

$D = 0,12287 m = 122,87 mm$


$Q = \left ( \frac{25 \cdot 140^{1,852} \cdot 0,12287^{4,871}}{10,643 \cdot 120} \right )^{\frac{1}{1,852}} = 0,045 m^3/s = 45 L/s$

```




```{r video-equivalente-paralelo, echo=FALSE}

knitr::include_url("https://www.youtube.com/embed/HW5oiCnOu3I")

```
